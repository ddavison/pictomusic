# Pictomusic

> Convert pictures into music!


## Develop

```bash
$ brew install imagemagick@6 && brew link imagemagick@6 --force
$ brew install libsndfile
```
