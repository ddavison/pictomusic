# frozen_string_literal: true

require 'spec_helper'

describe Pictomusic::Converter do
  subject { described_class.new('spec/fixtures/Flower_Hat.jpg') }
  let(:file_path) { File.expand_path('Flower_Hat.wav') }

  before do
    File.unlink(file_path) if File.exist?(file_path)
  end

  after do
    File.unlink(file_path) if File.exist?(file_path)
  end

  describe '.convert!' do
    it 'converts an image to a file' do
      subject.convert!
      expect(File.exist?(file_path)).to be true
    end

    xit 'can overwrite an existing converted file' do
      subject.convert!
      expect(File.exist?(file_path)).to be true
      modified_date = File::Stat.new(file_path).mtime

      subject.convert!
      new_modified_date = File::Stat.new(file_path).mtime

      expect(new_modified_date).to be > modified_date
    end
  end
end
