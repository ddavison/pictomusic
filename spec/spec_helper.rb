# frozen_string_literal: true

require File.expand_path('../lib/pictomusic', __dir__)

def teardown
  Dir['./*.wav'].each do |f|
    File.unlink(f)
  end
end
