# frozen_string_literal: true

require 'spec_helper'

describe Pictomusic::Command do
  let(:command) { spy('Pictomusic::Command') }

  let(:args) { %w[spec/fixtures/Flower_Hat.jpg] }
  let(:options) do
    { image: 'spec/fixtures/Flower_Hat.jpg' }
  end

  before do
    stub_const('Pictomusic::Command', command)
    allow(command).to receive(:initialize).with(args)
  end

  after do
    teardown
  end

  describe '.initialize' do
    context 'with arguments' do
      describe '--version' do
        let(:version) { Pictomusic::VERSION }

        it 'prints out the version' do
          expect { described_class.new(%w[--version]) }.to output(/#{version}/).to_stdout
        end
      end

      describe '--help' do
        it 'prints out the usage' do
          expect { described_class.new(%w[--help]) }.to output(/Usage/).to_stdout
          expect { described_class.new(%w[-h]) }.to output(/Usage/).to_stdout
        end
      end

      describe '--output-to' do
        let(:args) { %w[spec/fixtures/Flower_Hat.jpg --output-to=a_file.wav] }

        it 'outputs to a file' do
          described_class.new(args)
          expect(File).to exist('a_file.wav')
        end
      end
    end

    context 'no arguments' do
      it 'should quit out' do
        -> { described_class.new([]) }.should raise_error(SystemExit)
      end
    end
  end
end
