# frozen_string_literal: true

require 'spec_helper'

describe Pictomusic::VERSION do
  let(:semver) { subject.split(/\./) }

  it 'returns a semver string' do
    expect(subject).to be_a(String)
    expect(semver.size).to eq(3)

    3.times do |v|
      expect(semver[v]).to match(/\d+/)
    end
  end
end
