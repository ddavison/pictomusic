# frozen_string_literal: true

require 'logger'

module Pictomusic
  ###
  # Logger module intended to be prepended onto the Converter
  ###
  module Logger
    def convert!
      msg = ['Converting...']
      msg << "Image: #{image}"
      msg << "Destination: #{destination}"

      log(msg.join("\n\t"))

      super

      log('Conversion complete.')
    end

    private

    def logger
      @logger ||= ::Logger.new(STDOUT)
    end

    def log(message, level = :info)
      logger.send(level, message)
    end
  end
end
