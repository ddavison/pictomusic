# frozen_string_literal: true

require 'optparse'

module Pictomusic
  ###
  # CLI entrypoint class
  #   Called from bin/pictomusic
  ###
  class Command
    # @param [Array] argv: Command-line args passed in (ARGV)
    def initialize(argv) # rubocop:disable Metrics/MethodLength
      options = {}

      OptionParser.new do |opts|
        opts.banner = usage

        opts.on('--version', 'Show version') do |_|
          require_relative 'version'
          $stdout.puts("Pictomusic version #{Pictomusic::VERSION}")
          options[:version] = true
        end

        opts.on('-o', '--output-to=PATH', 'Output to file') do |o|
          options[:destination] = o
        end

        opts.on('-h', '--help', 'Show this usage message') do |_|
          $stdout.puts(opts)
          options[:help] = true
        end

        warn(opts) | exit(1) unless argv.size >= 1
      end.parse!(argv)

      options[:image] = argv.pop

      run!(options) unless options[:help] || options[:version]
    end

    private

    def run!(options)
      require_relative 'converter'
      Pictomusic::Converter.new(options[:image], destination: options[:destination]).tap do |converter|
        converter.destination = options[:destination] if options[:destination]
        converter.convert!
      end
    end

    def usage
      <<~USAGE
        Usage: #{$PROGRAM_NAME} picture [options]
      USAGE
    end
  end
end
