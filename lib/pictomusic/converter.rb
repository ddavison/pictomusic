# frozen_string_literal: true

require 'rmagick'
require 'sndfile'
require 'gsl-ng/matrix'

module Pictomusic
  ###
  #
  ###
  class Converter
    prepend Pictomusic::Logger
    include Magick

    attr_accessor :image, :destination

    # @param [String] image: The image to convert to sound
    # @param [String] destination: Destination to save the audio file
    def initialize(image, destination: nil)
      @image = File.expand_path(image) || ''
      @destination = destination || File.basename(@image)
    end

    def convert!
      image = Image.read(@image).first

      _, song_matrix = get_pixel_matrix(image)

      Sndfile::File.open("#{@destination.gsub(/\.([A-Z]|[a-z])+$/, '')}.wav", mode: :WRITE, format: :WAV, encoding: :PCM_16) do |song|
        song.write(song_matrix)
      end
    end

    # @param [Image] image
    # @return [Array] pixels: [0] The pixel map [1] The song matrix
    def get_pixel_matrix(image)
      pixels = image.get_pixels(0, 0, image.columns, image.rows).map do |px|
        [px.red * 255, px.green * 255, px.blue * 255]
      end

      song_matrix = GSLng::Matrix.from_array(pixels).transpose

      [pixels, song_matrix]
    end
  end
end
